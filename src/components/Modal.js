import React, { Component } from "react";
import {
      Button,
      Modal,
      ModalHeader,
      ModalBody,
      ModalFooter,
      Form,
      FormGroup,
      Input,
      Label
} from "reactstrap";

 export default class CustomModal extends Component {
      constructor(props) {
        super(props);
        this.state = {
          activeItem: this.props.activeItem
        };
      }
      handleChange = e => {
        let { name, value } = e.target;
        console.log(name,value)
        if (e.target.type === "checkbox") {
          value = e.target.checked;
        }
        const activeItem = { ...this.state.activeItem, [name]: value };
        this.setState({ activeItem });
      };
      render() {
        const { toggle, onSave } = this.props;
        return (
          <Modal isOpen={true} toggle={toggle}>
            <ModalHeader toggle={toggle}> Todo Item </ModalHeader>
            <ModalBody>
              <Form>
                <FormGroup>
                  <Label for="writing">writing</Label>
                  <Input
                    type="text"
                    name="writing"
                    value={this.state.activeItem.writing}
                    onChange={this.handleChange}
                    placeholder="Enter Writing"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="phonetic">phonetic</Label>
                  <Input
                    type="text"
                    name="phonetic"
                    value={this.state.activeItem.phonetic}
                    onChange={this.handleChange}
                    placeholder="Enter phonetic"
                  />
                </FormGroup>
                <FormGroup >
                  <Label for="meaning">meaning</Label>
                    <Input
                      type="text"
                      name="meaning"
                      checked={this.state.activeItem.meaning}
                      onChange={this.handleChange}
                      placeholder="Enter meaning"
                    />
                </FormGroup>
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button color="success" onClick={() => onSave(this.state.activeItem)}>
                Save
              </Button>
            </ModalFooter>
          </Modal>
        );
      }
    }