
// import React, { Component } from "react";
import React, {Component} from "react";
import Modal from './components/Modal';
import './App.css'
import axios from "axios"
// import jquery from "jquery"

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

var vocabWords, numVocabWords;
var flashcard = document.getElementById('flashcard');
var refreshBtn = document.getElementsByClassName('refresh')[0];
var enContent = document.getElementById('flashcard--content_en');
var esContent = document.getElementById('flashcard--content_es');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      viewCompleted: false,
      activeItem: {
        writing: "",
        phonetic: "",
        meaning: ""
      },
      clicked: false,
      currentFontColor : "",
      currentBackgroundColor: "",
      container : null,
      words: null,
      currentWord : null,
      currentPhonetic : null,
      currentWriting : null,
      index : 0,

     
    };
    this.container = null
    this.Color2 = "#85144B"
    this.Color1 = "#7FDBFF"
  }

  currPhone = "ha"
  currWrite = "he"

  componentDidMount(){
    axios.get('https://farsi-flashcard.herokuapp.com/api/words/')
    .then( res => {
      this.setState({
        index : Math.floor(Math.random()*res.data.length)
      })
      this.setState({
        words : res.data,
        currentWord: res.data[this.state.index],

      })
      this.setState({
        currentPhonetic: this.state.currentWord.meaning,
        currentWriting : ""
      },
      () => {
        
      })  
   
     console.log(this.state.index)
   

    })
    .catch( err =>{
      
      alert("error gan")

    })
  }

  handleNext = (event) =>{
    event.persist()
    
    
    this.setState((state) =>{
     state.index = this.modulo(state.index,0)
     state.currentWord = state.words[state.index]
    //  state.currentPhonetic = state.currentWord.meaning
    //  state.currentWriting = ""
     
     
    },
    () =>{
      this.setCard()
      

    })

    // this.setCard()

  }

  modulo = (number,type) =>{
    if (type == 0){
      return (((number%this.state.words.length)+this.state.words.length)+1)%this.state.words.length
    }
    return (((number%this.state.words.length)+this.state.words.length)-1)%this.state.words.length

    
  }

  handlePrevious = (event) =>{
    event.persist()

    this.setState((state) =>{
      state.index = this.modulo(state.index,1)
      state.currentWord = state.words[state.index]
     //  state.currentPhonetic = state.currentWord.meaning
     //  state.currentWriting = ""
      
     },
     () =>{
       this.setCard()
     })

    // this.setState({
    //   index : this.modulo(this.state.index,1)
    // })
    
    // this.setCard()
    

  }

  setCard = () =>{
    this.setState({
      currentWord : this.state.words[this.state.index]
    })
    this.setState({
      currentPhonetic: this.state.currentWord.meaning,
      currentWriting : ""
    })
   
  }

 

  handleClick = event => {
    event.persist()
    console.log(event)
    
    this.setState({
      clicked : !this.state.clicked 
    })
   
   
    this.setState((state) => {
      console.log(state)
      
      state.currentFontColor = state.clicked ? this.Color1 : this.Color2
      state.currentBackgroundColor =  state.clicked ? this.Color2 : this.Color1
      state.currentPhonetic =  state.clicked ? this.state.currentWord.phonetic : this.state.currentWord.meaning
      state.currentWriting = state.clicked ? this.state.currentWord.writing : ""
      

    },() => {

      event.target.style.color = this.state.currentFontColor
      event.target.style.backgroundColor = this.state.currentBackgroundColor
    
  
    })
    
    
  }

  createItem = () => {
    const item = { writing: "", phonetic: "", meaning: "" };
    this.setState({ activeItem: item, modal: !this.state.modal });
  };

  toggle = () => {
    this.setState({ modal: !this.state.modal });
  };

  

  handleSubmit = item => {
    console.log(item)
    let writingPattern =  new RegExp("^[\u0600-\u06FF]")
    let writingValid = writingPattern.test(item.writing)
    let latinPattern =  new RegExp("^[a-zAZ]")
    let meaningValid = latinPattern.test(item.meaning) && item.writing.length>0
    let phoneticValid = latinPattern.test(item.phonetic) && item.phonetic.length>0
    let valid = writingValid && meaningValid && phoneticValid
    let url = 'https://farsi-flashcard.herokuapp.com/api/addword'
    let data = JSON.stringify(item)
   
    
    if (valid){
      axios.post(url,data).then(res => {
        alert("save" + JSON.stringify(item));
      
        this.toggle();
      }).catch( err => {
        alert("failed to post")
      })
      
      
  
    }else{
      
      alert("Error occured, please try submitting again" + JSON.stringify(item))

      


    }
    
  };

 

  
  
 
  render() {
    
    
    return (
      <main className="content">
        
        <h1 className=" text-uppercase text-center my-4" >Persian Word Flashcard</h1>

        <span>
        <button onClick={this.createItem} className="btn btn-primary">Add task</button>
        </span>

           <div class="flip-container" id="flashcard">
                <span>
                <button onClick={this.handlePrevious} className="btn btn-primary">&laquo; Previous</button>
                </span>
                <span>
                <button onClick={this.handleNext} className="btn btn-primary">Next &raquo; </button>
                </span>
                <span>
                <button onClick={this.createItem} className="btn btn-primary">&laquo; Random &raquo;</button>
                </span>
              <div class="flipper" >
              <div class="front" onClick={this.handleClick} >
                            <span>{this.state.currentPhonetic}</span>
                             <br/>
                            {this.state.currentWriting}
              </div>
              <div class="back">
                <span id="flashcard--content_es">almendras</span>
              </div>
            </div>
          </div>
         
          
     
        {this.state.modal ? (
          <Modal
            activeItem={this.state.activeItem}
            toggle={this.toggle}
            onSave={this.handleSubmit}
          />
        ) : null}
      </main>
    );
  }
}
export default App;